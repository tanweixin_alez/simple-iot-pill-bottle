# Simple IoT medication container

Implementation of Bluetooth LE communications between Ubuntu and ESP32 with VL6180X Time-of-Flight (ToF) proximity sensor using Qt Creator

## Getting Started

### Prerequisites

* Get an ESP32 microcontroller, I used the DevKitC  
(https://www.espressif.com/en/products/hardware/esp32-devkitc/overview)

* Get the VL6180X ToF sensor, I used the SATEL which comes with a mini-PCB  
(https://www.st.com/en/evaluation-tools/vl6180x-satel.html)

* Get a simple pushbutton switch  
(something like https://alexnld.com/product/140pcs-14-types-momentary-tact-tactile-push-button-switch-smd-assortment-kit-set/)

* Wire everything up as described in pdf  
(https://bitbucket.org/tanweixin_alez/simple-iot-pill-bottle/src/master/BE3M38SPD_final_report_TANWeiXin.pdf)

### Installing

Last tested on Ubuntu 16.04.4 with Qt 5.11.3

* Install Arduino IDE on Ubuntu  
(https://www.arduino.cc/en/guide/linux)

* Configure the Arduino IDE to work with the ESP32  
(https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/)

* Install the VL6180X library by Pololu, I used version 1.2.0  
(https://github.com/pololu/vl6180x-arduino)

* Get the latest ESP32 BLE development version, default one incorporated into Arduino IDE did not work for this project (Jan 2019)  
(https://github.com/nkolban/ESP32_BLE_Arduino)

* Install Qt5 on Ubuntu  
(https://wiki.qt.io/Install_Qt_5_on_Ubuntu)

* Clone repository into your target directory

## Running the program

1. Connect the ESP32 to your PC and run the Arduino IDE.

2. Verify and upload IoT_pill_bottle.ino to the ESP32.

3. Run Qt Creator on Ubuntu as super user

    ```
    cd path/to/installation/dir/Qt/Tools/QtCreator/bin
    ```
    
    ```
    gksudo ./qtcreator
    ```

4. Open the BLE-server project in Qt Creator and build it.

5. Run the project

## Versioning

1.0.0

## Authors

* **'Alez' TAN Wei Xin**

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

This project is licensed under the **[MIT license](http://opensource.org/licenses/mit-license.php)**

## Acknowledgments

* 菲菲 (https://bitbucket.org/sfelixt/) for introducing me to Qt
* BE3M38SPD lecturers and TAs for providing the ESP32 and VL6180X-SATEL

