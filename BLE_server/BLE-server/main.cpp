#include <QtBluetooth/qlowenergyadvertisingdata.h>
#include <QtBluetooth/qlowenergyadvertisingparameters.h>
#include <QtBluetooth/qlowenergycharacteristic.h>
#include <QtBluetooth/qlowenergycharacteristicdata.h>
#include <QtBluetooth/qlowenergydescriptordata.h>
#include <QtBluetooth/qlowenergycontroller.h>
#include <QtBluetooth/qlowenergyservice.h>
#include <QtBluetooth/qlowenergyservicedata.h>
#include <QtCore/qbytearray.h>
#include <QtCore/qcoreapplication.h>
#include <QtCore/qlist.h>
#include <QtCore/qloggingcategory.h>
#include <QtCore/qscopedpointer.h>
#include <QtCore/qtimer.h>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QBarSeries>
#include <QtCharts/QValueAxis>

#include <QTime>

#define EMPTYMEAS 115.0
#define FULLMEAS 45.0

int main(int argc, char *argv[])
{
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.bluetooth* = true"));
    //! [CoreApplication for console only application]
//    QCoreApplication app(argc, argv);
    QApplication app(argc, argv);

    //! [Advertising Data]
    QLowEnergyAdvertisingData advertisingData;
    advertisingData.setDiscoverability(QLowEnergyAdvertisingData::DiscoverabilityGeneral);
    advertisingData.setIncludePowerLevel(true);
    advertisingData.setLocalName("BLE-server");
    // Bluetooth base UUID 00000000-0000-1000-8000-00805F9B34FB, serv_in_hex = 0xabcd1234 will simply change it to abcd1234-0000-1000-8000-00805F9B34FB
//    const quint32 serv_in_hex = 0x73657276;
//    advertisingData.setServices(QList<QBluetoothUuid>() << QBluetoothUuid(serv_in_hex));
    advertisingData.setServices(QList<QBluetoothUuid>() << QBluetoothUuid::HeartRate);
    //! [Advertising Data]

    //! [for printing out UUIDS]
//    const quint32 test = 6157;
//    qDebug() << QBluetoothUuid(test);
//    const quint32 test2 = 10807;
//    qDebug() << QBluetoothUuid(test2);
    //! [for printing out UUIDS]

    //! [Service Data]
    QLowEnergyCharacteristicData charData;
//    const quint32 char_in_hex = 0x63686172;
//    charData.setUuid(QBluetoothUuid(char_in_hex));
    charData.setUuid(QBluetoothUuid::HeartRateMeasurement);
    charData.setValue(QByteArray::number(FULLMEAS));
    charData.setProperties(QLowEnergyCharacteristic::Write);
    const QLowEnergyDescriptorData clientConfig(QBluetoothUuid::ClientCharacteristicConfiguration, QByteArray(2, 0));
    charData.addDescriptor(clientConfig);

    QLowEnergyServiceData serviceData;
    serviceData.setType(QLowEnergyServiceData::ServiceTypePrimary);
//    serviceData.setUuid(QBluetoothUuid(serv_in_hex));
    serviceData.setUuid(QBluetoothUuid::HeartRate);
    serviceData.addCharacteristic(charData);
    //! [Service Data]

    //! [Start Advertising]
    const QScopedPointer<QLowEnergyController> leController(QLowEnergyController::createPeripheral());
    const QScopedPointer<QLowEnergyService> service(leController->addService(serviceData));
    leController->startAdvertising(QLowEnergyAdvertisingParameters(), advertisingData, advertisingData);
    //! [Start Advertising]

    QLowEnergyCharacteristic characteristic = service->characteristic(QBluetoothUuid::HeartRateMeasurement);

    //![For plotting]
    QtCharts::QLineSeries *series = new QtCharts::QLineSeries();
    series->append(0.0,100.0);
    QtCharts::QBarSet *barset = new QtCharts::QBarSet("Opened");
    barset->append(0.0);
    barset->setColor(Qt::red);
    QtCharts::QBarSeries *barseries = new QtCharts::QBarSeries();
    barseries->append(barset);
    QtCharts::QChart *chart = new QtCharts::QChart();
    chart->addSeries(series);
    chart->addSeries(barseries);
    chart->setTitle("Pill bottle fill level");
    QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
    QMainWindow window;

    QtCharts::QValueAxis *axisX = new QtCharts::QValueAxis();
    axisX->setTitleText("Time since start of opertation, sec");
    axisX->setLabelFormat("%d");
    axisX->setMin(0.0);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QtCharts::QValueAxis *axisY = new QtCharts::QValueAxis();
    axisY->setTitleText("Fill level in percentage");
    axisY->setLabelFormat("%d");
    axisY->setMax(100.0);
    axisY->setMin(0.0);
    axisY->setTickCount(11);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
    //![For plotting]

    //! [Provide Data]
    QTimer updateTimer;
    QTime currTime = QTime::fromString("00:00", "hh:mm");
    double prevPercentage = 0.0;
    double fillPercentage = 100.0;
    const auto heartbeatProvider = [&characteristic, &leController, &currTime, &prevPercentage, &fillPercentage, &series, &barset, &barseries, &chart, &axisX, &axisY, &chartView, &window]()
    {
        Q_ASSERT(characteristic.isValid());

        QLowEnergyController::ControllerState state = leController->state();
        qDebug() << state;

        QString charStr = characteristic.value();
        qDebug() << charStr;

        currTime = currTime.addSecs(1);
        qDebug() << currTime.toString("hh:mm:ss");

        //! [Update plotter]
        double plotterTime = currTime.second();
        plotterTime += currTime.minute()*60.0;

        prevPercentage = fillPercentage;
        fillPercentage = 100.0 * ((charStr.toDouble() - EMPTYMEAS)/(FULLMEAS - EMPTYMEAS));

        if (fillPercentage < 0 || fillPercentage > 100)
        {
           qDebug() << "ERROR! Either sensor timeout or measurement needs recalibration";
        }
        else
        {
            //! [for debugging]
//            if(plotterTime > 1.0 && plotterTime < 8.0)
//            {
//                double randVal = random()/100000000;
//                if(randVal < 0.0)
//                    randVal *= -1;

//                qDebug() << randVal;

//                fillPercentage -= randVal;
//            }
            //! [for debugging]

            if( fabs(prevPercentage - fillPercentage) > 10.0 )
                barset->append(10.0);
            else
                barset->append(0.0);
            barseries->append(barset);

            series->append(plotterTime,fillPercentage);

            if( fmod(plotterTime,1.0) == 0 || plotterTime == 1.0 )
            {
                chart->removeSeries(series);
                chart->removeSeries(barseries);
                chart->legend()->hide();
                chart->addSeries(series);
                chart->addSeries(barseries);

                axisX->setMax(plotterTime);
                if(series->count() < 5)
                axisX->setTickCount(5);
                else
                axisX->setTickCount(series->count());
                series->attachAxis(axisX);
                series->attachAxis(axisY);
                barseries->attachAxis(axisX);
                barseries->attachAxis(axisY);

                chartView->setRenderHint(QPainter::Antialiasing);

                window.setCentralWidget(chartView);
                window.resize(1200, 900);
                window.show();
            }
            //! [Update plotter]
        }
    };
    QObject::connect(&updateTimer, &QTimer::timeout, heartbeatProvider);
    updateTimer.start(1000);
    //! [Provide Data]

    auto readvertise = [&leController, advertisingData, &serviceData, &characteristic]()
    {
        //! [have to redefine service otherwise service search fails for some reason]
        const QScopedPointer<QLowEnergyService> service2(leController->addService(serviceData));
        leController->startAdvertising(QLowEnergyAdvertisingParameters(), advertisingData, advertisingData);

        QByteArray prev(characteristic.value());
        characteristic = service2->characteristic(QBluetoothUuid::HeartRateMeasurement);
        service2->writeCharacteristic(characteristic, prev);
    };
    QObject::connect(leController.data(), &QLowEnergyController::disconnected, readvertise);

    return app.exec();
}
